package com.virtage.edu.e4.quiz.handlers;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.prefs.BackingStoreException;

import com.virtage.edu.e4.quiz.QuizConstants;

@SuppressWarnings("restriction")	// ignore unofficial Preferences API usage
public class OpenPreferencesHandler {

	@Execute
	public void execute(Shell shell, @Preference(nodePath=QuizConstants.PLUGIN_ID) IEclipsePreferences prefs) {
		
		boolean hideAnswers = MessageDialog.openQuestion(shell, QuizConstants.APP_NAME,
				"Would you like to hide answers?");
		
		prefs.putBoolean(QuizConstants.PREF_HIDE_ANSWERS, hideAnswers);
		
		try {
			prefs.flush();			// Save prefs
			
		} catch (BackingStoreException e) {
			MessageDialog.openError(shell, QuizConstants.APP_NAME, e.toString());
			
		}

	}
}