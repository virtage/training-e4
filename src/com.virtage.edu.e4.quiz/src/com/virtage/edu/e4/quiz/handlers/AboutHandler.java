 
package com.virtage.edu.e4.quiz.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.virtage.edu.e4.quiz.QuizConstants;

public class AboutHandler {
	
	@Execute
	public void execute(Shell parent) {
		String msg = QuizConstants.APP_NAME + "\n\n" +
				"Eclipse E4 training course sample application\n\n" +
				"Copyright (c) 2014 Virtage Software. www.virtage.cz";
		
		MessageDialog.openInformation(parent, QuizConstants.APP_NAME, msg);
	}
}