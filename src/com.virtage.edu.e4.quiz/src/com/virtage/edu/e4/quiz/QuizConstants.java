package com.virtage.edu.e4.quiz;

public interface QuizConstants {
	
	/** App name e.g. for dialog titles. */
	String APP_NAME = "Quiz - E4 demo app";
	
	/** System property name for QUIZDB. */
	String QUIZDB_PROPERTY = "quizdb";

	/** Event topic for "QUIZDB path changed". */
	String TOPIC_QUIZDB_CHANGED = "com/virtage/edu/e4/quiz/quizdb/CHANGED";

	/** Preference key whether to hide answers in quiz viewer */
	String PREF_HIDE_ANSWERS = "hideAnswers";

	
	String PLUGIN_ID = "com.virtage.edu.e4.quiz";
	
}
