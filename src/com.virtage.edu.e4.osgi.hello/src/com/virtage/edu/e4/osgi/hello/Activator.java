package com.virtage.edu.e4.osgi.hello;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("Hello from OSGi Hello bundle!");
	}
	
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		System.out.println("Goodbye from OSGi Hello bundle!");
	}

}
