package com.virtage.edu.e4.quiz.service.test;

import java.nio.file.Paths;
import java.util.Properties;

import com.virtage.edu.e4.quiz.service.IQuizService;

public class IQuizServiceTest {
	
	public void bind(IQuizService service) {
		System.out.println(IQuizService.class.getSimpleName() + " was bound by DS.");
		
		// test service
		assert System.getProperty("quizdb") != null : "Missing quizdb system property!";
		
		System.out.println("Reading quiz...");
		String path = System.getProperty("quizdb") + "/capitals.properties";
		Properties quiz = service.readQuiz(Paths.get(path));
		System.out.println(quiz);
	}
	
	public void unbind(IQuizService service) {
		System.out.println(IQuizService.class + " was unbound by DS.");
	}
}
