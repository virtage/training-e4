package com.virtage.edu.e4.quiz.viewer;

import java.util.Map;

import org.eclipse.jface.viewers.ColumnLabelProvider;

public class QuestionColumnLabelProvider extends ColumnLabelProvider {
	
	@Override
	public String getText(Object element) {
		assert element instanceof Map.Entry;
		
		@SuppressWarnings("unchecked")
		Map.Entry<String, String> entry = (Map.Entry<String, String>) element;
		
		return entry.getKey();
	}

}
