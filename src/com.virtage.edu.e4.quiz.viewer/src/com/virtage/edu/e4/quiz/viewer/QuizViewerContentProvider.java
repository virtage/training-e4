package com.virtage.edu.e4.quiz.viewer;

import java.util.Map;
import java.util.Properties;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class QuizViewerContentProvider implements IStructuredContentProvider {

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	/**
	 * Expects Properties as input, converts it to array of {@link Map.Entry}.
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		assert inputElement instanceof Properties;
		
		Properties props = (Properties) inputElement;
		
		return props.entrySet().toArray(new Map.Entry[] {});
	}

}
