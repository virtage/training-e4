package com.virtage.edu.e4.quiz.viewer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.virtage.edu.e4.quiz.QuizConstants;
import com.virtage.edu.e4.quiz.service.IQuizService;

@SuppressWarnings("restriction")	// Ignore unofficial Preference API usage
public class QuizViewerPart {

	private TableViewer tableViewer;
		
	/** QuizService injected from OSGi Sevice Registry */
	@Inject	private IQuizService quizService;

	@PostConstruct
	public void postConstruct(Composite parent) {
		tableViewer = new TableViewer(parent, SWT.BORDER
				| SWT.FULL_SELECTION);
		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		tableViewer.setContentProvider(new QuizViewerContentProvider());
		//tableViewer.setContentProvider(new ArrayContentProvider());

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(
				tableViewer, SWT.NONE);
		TableColumn tblclmnNewColumn_1 = tableViewerColumn_1.getColumn();
		tblclmnNewColumn_1.setWidth(250);
		tblclmnNewColumn_1.setText("Question");
		tableViewerColumn_1.setLabelProvider(new QuestionColumnLabelProvider());
		

		TableViewerColumn tableViewerColumn = new TableViewerColumn(
				tableViewer, SWT.NONE);
		TableColumn tblclmnNewColumn = tableViewerColumn.getColumn();
		tblclmnNewColumn.setWidth(250);
		tblclmnNewColumn.setText("Answer");
		tableViewerColumn.setLabelProvider(new AnswerColumnLabelProvider());
		
	}
	
	@Inject
	public void setInput(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) Path quizFile) {
		if (quizFile != null) {
			Properties props = quizService.readQuiz(quizFile);
			tableViewer.setInput(props);
		}
	}
	
	@Inject	@Optional
	public void setHideAnswers(
		@Preference(nodePath = QuizConstants.PLUGIN_ID,
					value = QuizConstants.PREF_HIDE_ANSWERS)
					boolean hideAnswers) {
		
		if ((tableViewer != null) && (!tableViewer.getControl().isDisposed())) {

			// Hide/unhide "Answer" column according to preference key change
			if (hideAnswers) {
				tableViewer.getTable().getColumn(1).setWidth(0);

			} else {
				tableViewer.getTable().getColumn(1).setWidth(250);

			}
		}
	}
	
	

	// TODO: Rewrite to JUnit test later
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		
		
		
		// Prepare and test
		QuizViewerPart part = new QuizViewerPart();
		part.postConstruct(shell);
		
		Path path = Paths.get(System.getProperty("quizdb") + "/capitals.properties");
		
		Properties props = new Properties();
		try (InputStream is = Files.newInputStream(path)) {
			props.load(is);
			
		} catch (IOException e) {
			props.put("An error occured", e.toString());
		}
		
		part.tableViewer.setInput(props);
		
		
		
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}