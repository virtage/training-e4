package com.virtage.edu.e4.quiz.service.internal;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.virtage.edu.e4.quiz.service.IQuizService;

public class QuizServiceImpl implements IQuizService {

	@Override
	public Properties readQuiz(Path path) {
		
		Properties props = new Properties();
		
		try (InputStream is = Files.newInputStream(path)) {
			props.load(is);
			
		} catch (IOException e) {
			props.put("An error occured", e.toString());
		}
		
		return props;
	}
	
	// TODO: Rewrite to JUnit test
	public static void main(String[] args) {
		QuizServiceImpl service = new QuizServiceImpl();
		Properties quiz = service.readQuiz(Paths.get(System.getProperty("quizdb") + "/capitals.properties"));
		
		System.out.println(quiz);
	}

}
