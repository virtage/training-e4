package com.virtage.edu.e4.quiz.service;

import java.nio.file.Path;
import java.util.Properties;

public interface IQuizService {
	
	/**
	 * Read quiz file from specified filesystem path.
	 * 
	 * @param path
	 * @return Quiz as properties
	 */
	Properties readQuiz(Path path);
	
}
