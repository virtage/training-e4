package com.virtage.edu.e4.quiz.explorer;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;

import com.virtage.edu.e4.quiz.QuizConstants;

public class OpenQuizHandler {
	
	@Execute
	public void execute(Shell shell, IEventBroker eventBroker) {
		// MessageDialog.openError(shell, QuizConstants.APP_NAME,
		// "Not implemented yet");

		// ** Directly standard selection **
		DirectoryDialog dirDialog = new DirectoryDialog(shell);
		
		// * Initial folder to QUIZDB property *
		// from system property
		dirDialog.setFilterPath(System.getProperty(QuizConstants.QUIZDB_PROPERTY));
		
		dirDialog.setText("Select your QUIZDB folder");
		String selectedDir = dirDialog.open();
		
		// If something selected
		if (selectedDir != null) {
			Path path = Paths.get(selectedDir);
			// a) setInput() made static - nothing fancy
			//ExplorerPart.setInput(path);
			
			// b) Send message via Eclipse 4 Event System
			eventBroker.post(QuizConstants.TOPIC_QUIZDB_CHANGED, path);
		}
	}

}