 
package com.virtage.edu.e4.quiz.explorer;

import java.nio.file.Path;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.widgets.Composite;

import com.virtage.edu.e4.quiz.QuizConstants;

public class ExplorerPart {
	
	private ListViewer listViewer;
	
	@Inject
	private ESelectionService selectionService;

	@PostConstruct
	public void createControls(Composite parent) {
		listViewer = new ListViewer(parent);
		listViewer.setContentProvider(new PathContentProvider());
		listViewer.setLabelProvider(new PathLabelProvider());
		
		//viewer.setInput(Paths.get(System.getProperty(QuizConstants.QUIZDB_PROPERTY)));
		
		// Update global selection
		listViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) listViewer.getSelection();
				selectionService.setSelection(selection.getFirstElement());
			}
		});
	}
	
	@Inject @Optional
	public void setInput(@UIEventTopic(QuizConstants.TOPIC_QUIZDB_CHANGED) Path path) {
		listViewer.setInput(path);
	}
	
	@Focus
	public void onFocus() {}
	
}