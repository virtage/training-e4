package com.virtage.edu.e4.quiz.explorer;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.virtage.edu.e4.quiz.QuizConstants;

public class PathContentProvider implements IStructuredContentProvider {

	@Override
	public void dispose() {}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

	@Override
	public Object[] getElements(Object inputElement) {
		assert inputElement instanceof Path : "Not instance of Path!";
		
		Path input = (Path) inputElement;
		List<Path> children = new ArrayList<>();
		
		// Iterate trough folder content
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(input)) {
			for (Path child : ds) {
				
				// List only .properties files
				if (child.getFileName().toString().endsWith(".properties"))
					children.add(child);
			}
			
		} catch (IOException e) {
			MessageDialog.openError(null, QuizConstants.APP_NAME,
					"An error occured during loading folder content\n\n" + e);
		}
		
		if (children.isEmpty()) {
			MessageDialog.openWarning(null, QuizConstants.APP_NAME,
					"Folder does not contain any .properties files!");
		}
		
		// Convert list to array and return
		return children.toArray(new Path[] {});
	}

}
