package com.virtage.edu.e4.quiz.explorer;

import java.nio.file.Path;

import org.eclipse.jface.viewers.LabelProvider;

public class PathLabelProvider extends LabelProvider {
	
	@Override
	public String getText(Object element) {
		assert element instanceof Path : "Not instance of Path!";
		
		Path input = (Path) element;
		
		return input.getFileName().toString();
	}

}
